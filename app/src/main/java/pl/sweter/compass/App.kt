package pl.sweter.compass

import android.app.Application
import pl.sweter.compass.di.AppComponent
import pl.sweter.compass.di.DaggerAppComponent
import pl.sweter.compass.di.SensorModule
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : Application() {

  override fun onCreate() {
    super.onCreate()
    if (BuildConfig.DEBUG) {
      Timber.plant(DebugTree())
    }
    appComponent = DaggerAppComponent.builder()
      .sensorModule(SensorModule(this))
      .build()
  }

  companion object {
    lateinit var appComponent: AppComponent
  }
}