package pl.sweter.compass.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.sweter.compass.ui.CompassViewModel

@Module
abstract class ViewModelModule {

  @Binds
  internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

  @Binds
  @IntoMap
  @ViewModelKey(CompassViewModel::class)
  internal abstract fun compassViewModel(viewModel: CompassViewModel): ViewModel

}