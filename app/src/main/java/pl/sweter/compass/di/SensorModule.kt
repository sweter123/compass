package pl.sweter.compass.di

import android.content.Context
import androidx.lifecycle.LiveData
import dagger.Module
import dagger.Provides
import pl.sweter.compass.compass.CompassLiveData
import pl.sweter.compass.location.LocationLiveData
import pl.sweter.compass.model.GeoLocation

@Module
class SensorModule(private val context: Context) {

  @Provides
  fun provideCompassLiveData(): LiveData<Float> = CompassLiveData(context)

  @Provides
  fun locationLiveData(): LiveData<GeoLocation> = LocationLiveData(context)

}