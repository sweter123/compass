package pl.sweter.compass.di

import dagger.Component
import pl.sweter.compass.ui.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [SensorModule::class, ViewModelModule::class])
interface AppComponent {
  fun inject(mainActivity: MainActivity)
}