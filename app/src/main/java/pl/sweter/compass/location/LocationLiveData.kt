package pl.sweter.compass.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.annotation.RequiresPermission
import androidx.lifecycle.Lifecycle.Event.ON_START
import androidx.lifecycle.Lifecycle.Event.ON_STOP
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import pl.sweter.compass.model.GeoLocation

class LocationLiveData(context: Context) : LocationListener, LiveData<GeoLocation>() {

  private val locationManager by lazy { context.getSystemService(LOCATION_SERVICE) as LocationManager }


  @SuppressLint("MissingPermission")
  override fun onActive() {
    super.onActive()
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
    val lastLocation: Location? =
      locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) ?: locationManager.getLastKnownLocation(
        LocationManager.NETWORK_PROVIDER
      )
    lastLocation?.run {
      value = GeoLocation(latitude, longitude)
    }
  }

  override fun onInactive() {
    super.onInactive()
    locationManager.removeUpdates(this)
  }

  @RequiresPermission(anyOf = [Manifest.permission.ACCESS_FINE_LOCATION])
  override fun observe(owner: LifecycleOwner, observer: Observer<in GeoLocation>) {
    super.observe(owner, observer)
  }

  override fun onLocationChanged(location: Location) {
    value = GeoLocation(location.latitude, location.longitude)
  }

  override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
  }

  override fun onProviderEnabled(provider: String?) {
  }

  override fun onProviderDisabled(provider: String?) {
  }
}