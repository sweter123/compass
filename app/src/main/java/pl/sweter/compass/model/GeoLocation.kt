package pl.sweter.compass.model

data class GeoLocation(
  val latitude: Double,
  val longitude: Double
)