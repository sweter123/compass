package pl.sweter.compass.utils

import android.content.Context

fun Int.toPixels(context: Context): Int {
  return (this * context.resources.displayMetrics.density).toInt()
}