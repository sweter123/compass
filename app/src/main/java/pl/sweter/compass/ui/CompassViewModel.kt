package pl.sweter.compass.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import pl.sweter.compass.model.GeoLocation
import javax.inject.Inject

class CompassViewModel @Inject constructor(
  compassLiveData: LiveData<Float>,
  private val locationLiveData: LiveData<GeoLocation>
) : ViewModel() {

  var headingLocation: GeoLocation? = null
    set(value) {
      field = value
      calculateHeadingAzimuth()
    }

  private val headingAzimuthLiveData = MutableLiveData<Float>()

  val compass: LiveData<Float> = Transformations.map(compassLiveData) {
    -it
  }

  val headingAzimuth: LiveData<Float> = Transformations.switchMap(locationLiveData) {
    calculateHeadingAzimuth()
    headingAzimuthLiveData
  }

  private fun calculateHeadingAzimuth() {
    val currentLocation = locationLiveData.value
    val headingLocation = this.headingLocation
    if (currentLocation != null && headingLocation != null) {
      val latitude1 = Math.toRadians(currentLocation.latitude)
      val latitude2 = Math.toRadians(headingLocation.latitude)
      val longDiff = Math.toRadians(headingLocation.longitude - currentLocation.longitude)
      val y = Math.sin(longDiff) * Math.cos(latitude2)
      val x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff)
      headingAzimuthLiveData.value = Math.toDegrees(Math.atan2(y, x)).toFloat()
    }
  }

}