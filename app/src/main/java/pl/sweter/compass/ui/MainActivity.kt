package pl.sweter.compass.ui

import android.Manifest
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import androidx.appcompat.app.AlertDialog.Builder
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.compass_view
import kotlinx.android.synthetic.main.activity_main.input_button
import pl.sweter.compass.App
import pl.sweter.compass.R.layout
import pl.sweter.compass.R.string
import javax.inject.Inject

const val LOCATION_REQUEST_CODE = 1234
const val INPUT_DIALOG_TAG = "input"

class MainActivity : AppCompatActivity() {

  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory

  private val compassViewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(CompassViewModel::class.java) }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(layout.activity_main)
    App.appComponent.inject(this)
    checkLocationPermissions()
    observeSensorValues()
    input_button.setOnClickListener {
      LocationInputDialog { compassViewModel.headingLocation = it }
        .show(
          supportFragmentManager,
          INPUT_DIALOG_TAG
        )
    }
  }

  private fun checkLocationPermissions() {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      compassViewModel.headingAzimuth
        .observe(this, Observer {
          compass_view.headingRotation = it
          compass_view.headingEnabled = true
        })
    } else {
      ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
    }
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    if (requestCode == LOCATION_REQUEST_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      checkLocationPermissions()
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
  }

  private fun observeSensorValues() {
    if (hasNecessarySensors()) {
      compassViewModel.compass
        .observe(this, Observer { compass_view.compassRotation = it })
    } else {
      Builder(this)
        .setTitle(string.error_dialog_title)
        .setMessage(string.no_sensor_error)
        .setPositiveButton(string.dialog_accept_button) { dialog, _ -> dialog.dismiss() }
        .create().show()
    }
    checkLocationPermissions()
  }

  private fun hasNecessarySensors(): Boolean {
    val sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
    return sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null ||
        (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null
            && sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
  }

}
