package pl.sweter.compass.ui

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import pl.sweter.compass.R
import pl.sweter.compass.utils.toPixels

class CompassView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

  private val compassBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.compass)
  private val headingBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.heading)

  private val compassRect = Rect()
  private val headingRect = Rect()

  var headingEnabled = false
    set(value) {
      field = value
      invalidate()
    }

  var compassRotation = 0f
    set(value) {
      field = value
      invalidate()
    }
  var headingRotation = 0f
    set(value) {
      field = value
      invalidate()
    }

  override fun onDraw(canvas: Canvas) {
    canvas.rotate(compassRotation, width / 2f, height / 2f)
    canvas.drawBitmap(compassBitmap, null, compassRect, null)
    if (headingEnabled) {
      canvas.rotate(headingRotation, width / 2f, height / 2f)
      canvas.drawBitmap(headingBitmap, null, headingRect, null)
    }
    super.onDraw(canvas)
  }

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    val parentSize = Math.min(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec))
    setMeasuredDimension(parentSize, parentSize)
    compassRect.set(paddingLeft, paddingTop, parentSize - paddingRight, parentSize - paddingBottom)
    headingRect.set(
      (parentSize / 2) - (24.toPixels(context) / 2),
      paddingTop - 8.toPixels(context),
      (parentSize / 2) + (24.toPixels(context)),
      42.toPixels(context) + paddingTop - 8.toPixels(context)
    )
  }

}