package pl.sweter.compass.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.location_input_dialog.dialog_input_button
import kotlinx.android.synthetic.main.location_input_dialog.latitude
import kotlinx.android.synthetic.main.location_input_dialog.latitude_input_layout
import kotlinx.android.synthetic.main.location_input_dialog.longitude
import kotlinx.android.synthetic.main.location_input_dialog.longitude_input_layout
import pl.sweter.compass.R
import pl.sweter.compass.model.GeoLocation
import java.lang.NumberFormatException

class LocationInputDialog(val listener: (GeoLocation) -> Unit) : DialogFragment() {

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.location_input_dialog, container, false)
  }

  override fun onStart() {
    super.onStart()
    dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    dialog_input_button.setOnClickListener {
      validateLatLng()
    }
  }

  private fun validateLatLng() {
    val lat: Double
    val lng: Double
    try {
      lat = latitude.text.toString().toDouble()
    } catch (e: NumberFormatException) {
      latitude_input_layout.error = getString(R.string.number_format_error)
      return
    }
    try {
      lng = longitude.text.toString().toDouble()
    } catch (e: NumberFormatException) {
      longitude_input_layout.error = getString(R.string.number_format_error)
      return
    }
    listener.invoke(GeoLocation(lat, lng))
    dialog?.dismiss()
  }

}