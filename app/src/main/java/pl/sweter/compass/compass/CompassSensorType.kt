package pl.sweter.compass.compass

enum class CompassSensorType{
  GYROSCOPE,
  ACCELEROMETER,
  NONE
}