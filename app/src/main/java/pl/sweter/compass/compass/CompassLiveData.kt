package pl.sweter.compass.compass

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import pl.sweter.compass.compass.CompassSensorType.ACCELEROMETER
import pl.sweter.compass.compass.CompassSensorType.GYROSCOPE
import pl.sweter.compass.compass.CompassSensorType.NONE
import timber.log.Timber

class CompassLiveData(context: Context) : LiveData<Float>(), SensorEventListener {

  private val sensorManager by lazy { context.getSystemService(AppCompatActivity.SENSOR_SERVICE) as SensorManager }
  private val gSensor: Sensor? by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
  private val magneticSensor: Sensor? by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) }
  private val rotationSensor: Sensor? by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) }

  private var rotationMatrix = FloatArray(9)
  private var orientation = FloatArray(3)

  private lateinit var lastAccelerometerData: FloatArray
  private lateinit var lastMagnetometerData: FloatArray

  private val compassType by lazy { determineSensorsAvailability() }

  override fun onInactive() {
    super.onInactive()
    sensorManager.unregisterListener(this)
  }

  override fun onActive() {
    super.onActive()
    when (compassType) {
      GYROSCOPE -> sensorManager.registerListener(
        this,
        rotationSensor,
        SensorManager.SENSOR_DELAY_GAME
      )
      ACCELEROMETER -> {
        sensorManager.registerListener(
          this,
          gSensor,
          SensorManager.SENSOR_DELAY_GAME
        )
        sensorManager.registerListener(
          this,
          magneticSensor,
          SensorManager.SENSOR_DELAY_GAME
        )
      }
      NONE -> {
      }
    }
  }

  override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
  }

  override fun onSensorChanged(event: SensorEvent) {
    when (compassType) {
      GYROSCOPE -> processGyroscopeData(event)
      ACCELEROMETER -> processAccelerometerData(event)
      NONE -> {
      }
    }

  }

  private fun determineSensorsAvailability(): CompassSensorType {
    return when {
      rotationSensor != null -> GYROSCOPE
      gSensor != null && magneticSensor != null -> ACCELEROMETER
      else -> NONE
    }
  }

  private fun processGyroscopeData(event: SensorEvent) {
    SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values)
    val orientation = SensorManager.getOrientation(rotationMatrix, orientation)
    val azimuth = Math.toDegrees(orientation[0].toDouble())
    value = azimuth.toFloat()
  }

  private fun processAccelerometerData(event: SensorEvent) {
    if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
      lastAccelerometerData = event.values.copyOf()
    } else if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
      lastMagnetometerData = event.values.copyOf()
    }
    if (::lastAccelerometerData.isInitialized && ::lastMagnetometerData.isInitialized) {
      SensorManager.getRotationMatrix(
        rotationMatrix,
        null,
        lastAccelerometerData,
        lastMagnetometerData
      )
      val orientation = SensorManager.getOrientation(rotationMatrix, orientation)
      val azimuth = Math.toDegrees(orientation[0].toDouble())
      value = azimuth.toFloat()
    }
  }

}