package pl.sweter.compass.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.MockitoAnnotations
import pl.sweter.compass.model.GeoLocation

class CompassViewModelTest {

  private val locationLiveData = MutableLiveData<GeoLocation>()
  private val compassLiveData = MutableLiveData<Float>()

  private val compassViewModel = CompassViewModel(compassLiveData, locationLiveData)

  @Mock lateinit var compassObserver: Observer<Float>
  @Mock lateinit var headingObserver: Observer<Float>

  @get:Rule
  var rule: TestRule = InstantTaskExecutorRule()

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
  }

  @Test
  fun `whenever compass sensor changes value, compassViewModel also changes compass azimuth value`() {
    compassViewModel.compass.observeForever(compassObserver)
    compassLiveData.value = 43f
    verify(compassObserver).onChanged(-43f)
  }

  @Test
  fun `whenever location changes and heading location is set, compassViewModel calculates new heading azimuth`() {
    compassViewModel.headingLocation = GeoLocation(55.0, 23.0)
    compassViewModel.headingAzimuth.observeForever(headingObserver)
    locationLiveData.value = GeoLocation(52.0, 23.0)
    verify(headingObserver).onChanged(0f)
  }

  @Test
  fun `whenever location changes but heading location is not set, new heading azimuth is not calculated`(){
    compassViewModel.headingAzimuth.observeForever(headingObserver)
    locationLiveData.value = GeoLocation(52.0, 23.0)
    verifyNoMoreInteractions(headingObserver)
  }

  @Test
  fun `whenever user sets heading location and current location is set, compassViewModel calculates new heading azimuth`() {
    locationLiveData.value = GeoLocation(52.0, 23.0)
    compassViewModel.headingAzimuth.observeForever(headingObserver)
    compassViewModel.headingLocation = GeoLocation(55.0, 23.0)
    verify(headingObserver).onChanged(0f)
  }

  @Test
  fun `whenever heading location changes but current location is not set, new heading azimuth is not calculated`(){
    compassViewModel.headingAzimuth.observeForever(headingObserver)
    locationLiveData.value = GeoLocation(52.0, 23.0)
    verifyNoMoreInteractions(headingObserver)
  }

}